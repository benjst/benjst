## About me

Hello, 

I'm a software test engineer located in Innsbruck, Austria
with background in physics and mathematics. 

I'm interested in programming language designs, network database concepts, distributed computing and machine learning.

In my spare time, I'm working on a network idea that can be used as information storage and processing.

I'm also a passionate swing dancer.

## Upcoming things

- A little personal website about me, my interests, projects, useful links and maybe a blog.
- A text-based interface on my website to the network to demonstrate its usefulness.
